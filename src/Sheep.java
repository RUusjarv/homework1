public class Sheep {

	enum Animal {
		sheep, goat
	};

	public static void main(String[] param) {
		reorder(Animal.values());
	}

	public static void reorder(Animal[] animals) {
		if (animals.length < 2)
			return; // kontrollib ka �ldse midagi sorteerida on
		int j=animals.length-1; //viimane
		for (int i= 0; i<j;){ //k�ib kokkup�rkeni l�bi
			if (animals[i]== Animal.goat){//kui on esimesel kohal goat, siis j��b sinna ning v�tame j�rgmise koha ette
			i++;	// 
			}
			if (animals[j]== Animal.sheep){//kui viimasel kohal sokk, siis j��b sinna ning v�tame eelmise ette
			j--;	
			}
			else{//ainuke v�imalus j��nud, et on vastupidi, siis vahetame kohad
				Animal temp = animals[i];
				animals[i] = animals [j];
				animals[j] = temp;
			}
		}
	}
}
